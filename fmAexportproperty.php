<?php
	require('config/conn.php');

$strExcelFileName="ทรัพย์สินทั้งหมด.xls";
header("Content-Type: application/x-msexcel; name=\"$strExcelFileName\"");
header("Content-Disposition: inline; filename=\"$strExcelFileName\"");
header("Pragma:no-cache");

$SQLProperty = "SELECT property.pro_date,property.pro_pic,category.cat_name,property.pro_id,property.pro_name,property.pro_detail,property.pro_status,property.pro_color,property.pro_date,property.pro_price,property.pro_note,branch.br_name,department.dep_name FROM property INNER JOIN department INNER JOIN branch INNER JOIN category WHERE property.dep_id=department.dep_id and property.br_id=branch.br_id and property.cat_id=category.cat_id and pro_status NOT IN ('3');";
$result=mysqli_query($conn,$SQLProperty);
?>
<html xmlns:o="urn:schemas-microsoft-com:office:office"xmlns:x="urn:schemas-microsoft-com:office:excel"xmlns="http://www.w3.org/TR/REC-html40">
 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<br>
<div id="SiXhEaD_Excel" align=center x:publishsource="Excel">
	<table x:str class="table table-striped table-bordered" border="1" >
		<thead>
				<tr class="text-center">
					<th>รหัสทรัพย์สิน</th>
					<th>รายการ (ชื่อ รายละเอียด สี)</th>
					<th>ราคา(บาท)</th>
					<th>วันที่ได้มา</th>
					<th>ประเภท</th>
					<th>สาขา</th>
					<th>แผนก</th>
					<th>สถานะ</th>					
				</tr>
		</thead>
		<tbody>
			<?php while ($data = mysqli_fetch_assoc($result)) {?>
				<tr>
					<td style="width:4%"><?php echo $data['pro_id']; ?></td>
					<td style="width:18%" ><?php echo $data['pro_name']; ?> <?php echo $data['pro_detail']; ?> <?php echo $data['pro_color']; ?></td>
					<td style="width:2%" ><?php echo number_format( $data['pro_price'],2); ?></td>
					<td style="width:8%" class="text-center"><?php echo $data['pro_date']; ?></td>
					<td style="width:5%" ><?php echo $data['cat_name']; ?></td>
					<td style="width:5%" ><?php echo $data['br_name']; ?></td>
					<td style="width:8%" ><?php echo $data['dep_name']; ?></td>
					<td style="width:8%" >
					<?php
								if ($data["pro_status"] == "0") {
									$level = "ใช้งานได้";
									echo "<font color=\"green\">$level</font>"; 
								} else if ($data["pro_status"] == "1") {
									$level = "ชำรุด";
									echo "<font color=\"red\">$level</font>"; 
								} else if ($data["pro_status"] == "2"){
									$level = "กำลังถูกยืม";
									echo "<font color=\"red\">$level</font>"; 
								}else{
									$level = "รอการอนุมัติ";
									echo "<font color=\"Orange\">$level</font>"; 
								}
								
					?>
				</tr>
			<?php
			}
			?>
		</tbody>
</table>
</div>


<script>
window.onbeforeunload = function(){return false;};
setTimeout(function(){window.close();}, 10000);
</script>
</body>
</html>