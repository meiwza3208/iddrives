<?php
    session_start();
	if (!$_SESSION['ad_user']){
		header("Location: /iddrivesgit/index.php");
	}
	require('config/conn.php');
?>

<?php

$ad_userName = $_SESSION['ad_user'];
$sqlUser = "SELECT admin.ad_name,department.dep_name,department.dep_id,branch.br_id,branch.br_name FROM admin INNER JOIN department INNER JOIN branch ON admin.dep_id = department.dep_id and admin.br_id=branch.br_id WHERE ad_user='$ad_userName' ";
$resultsqlUser = mysqli_query($conn, $sqlUser);
$num_rows = mysqli_num_rows($resultsqlUser);
$rowsqlUser =  mysqli_fetch_array($resultsqlUser);
$ad_name = $rowsqlUser["ad_name"];
$dep_name = $rowsqlUser["dep_name"];
$dep_id = $rowsqlUser["dep_id"];
$br_id = $rowsqlUser["br_id"];
$br_name = $rowsqlUser["br_name"];


$SQLProperty = "SELECT * FROM property 
LEFT JOIN category ON category.cat_id = property.cat_id  
LEFT JOIN department ON department.dep_id = property.dep_id
WHERE property.dep_id='$dep_id' ";
$rows=mysqli_query($conn,$SQLProperty);

?>
<!DOCTYPE html>
<html lang="en"><!-- Basic -->
<head>

	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">   
   
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
 
     <!-- Site Metas -->
    <title>STAFF</title>  
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="images/logo1.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
	
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">    
	<!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css">    
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
		

</head>
<style>
.tbl_data_filter {
	text-align: end !important;
}
#tbl_data_filter {
	text-align: end !important;
}
</style>
<script>
		$(document).ready(function() {
	    	$('#tbl_data').DataTable();
		} );
</script>

<!--start real time-->

<!--end real time-->

<body>
	<!-- เริ่มดัก ERROR -->
	<div>
		<?php if (isset($_SESSION["process_success"])) : ?>
			<div class="alert alert-success">
				<?php echo $_SESSION["process_success"];
				unset($_SESSION["process_success"]);
				?>

			</div>
		<?php elseif (isset($_SESSION["process_error"])) : ?>
			<div class="alert alert-danger">
				<?php echo $_SESSION["process_error"];
				unset($_SESSION["process_error"]);
				?>
			</div>
		<?php endif ?>
	</div>
	<!-- จบดัก ERROR -->
	<!-- Start header -->
	<header class="top-navbar">
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<div class="container">
				<a class="navbar-brand">
					<a href="staff.php" ><img src="images/Logo.png"class="rounded-circle" alt="Cinque Terre" width="100"/>
				</a>
			
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbars-rs-food" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
				  <span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbars-rs-food">
					<ul class="navbar-nav ml-auto">
						<li class="nav-item "><a class="nav-link" href="staff.php">หน้าหลัก</a></li>
						<li class="nav-item "><a class="nav-link" href="Sproperty.php">ข้อมูลทรัพย์สิน</a></li>
						<li class="nav-item active"><a class="nav-link" href="Sborrow.php">ยืมทรัพย์สิน</a></li>
						<li class="nav-item "><a class="nav-link" href="Sreturn.php">คืนทรัพย์สิน</a></li>
						<a class="nav-link">พนักงาน: คุณ<?php echo $ad_name;?></a><a class="nav-link" href="php\logout.php"><img src="images/iconlogout.png" width="30"><br></a>
					</ul>
				</div>
			</div>
		</nav>
	</header>
	<!-- End header -->

	<!-- Start All Pages -->
	<div class="all-page-title1 page-breadcrumb1">
		<div class="container text-center">
			<div class="row">
				<div class="col-lg-12">
				</div>
			</div>
		</div>
	</div>
	<!-- End All Pages -->

    <!-- เริ่มหมวดหมู่ -->
	<?php
		$bo_id = "BORROW" . date("Ymd") . "-" . random_int(1, 100);
	?> 
	<h1 align="center"><b>ยืมทรัพย์สิน</b></h1>
	<div class="container">
		<form action="php/addborrow.php" method="post" enctype="multipart/form-data">		
		<div class="row">
			
						<input class="form-control" type="hidden" value=<?php echo $ad_userName ?> id="ad_user" name="ad_user"  hidden>
						
						<input class="form-control" type="hidden" value=<?php echo $bo_id ?> id="bo_id" name="bo_id" readonly>
			
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
						<label >สาขา *</label>
						<input class="form-control" type="Text" id="br_name" name="br_Name" value = "<?php echo $rowsqlUser['br_name'];?>"readonly>
						<input class="form-control" type="hidden" id="br_id" name="br_id" value = "<?php echo $rowsqlUser['br_id'];?>">
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
						<label >แผนก *</label>
						<input class="form-control" type="Text" id="dep_Name" name="dep_Name" value = "<?php echo $rowsqlUser['dep_name'];?>"readonly>
						<input class="form-control" type="hidden" id="dep_id" name="dep_id" value = "<?php echo $rowsqlUser['dep_id'];?>">
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
						<label >ชื่อ-นามสกุลผู้ยืม *</label>
						<input class="form-control" type="Text" id="bo_name" name="bo_name" required>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
						<label >ประเภท *</label>
						<select name="cat_id" id="cat_id" class="form-control" onchange="getProperty()"  required>
						<option value="">-ประเภท-</option>
							<?php 
								$sql1 = "SELECT t2.cat_id,t2.cat_name FROM property t1 inner Join category t2 On t1.cat_id  = t2.cat_id where br_id = '".$br_id."' AND dep_id = '".$dep_id."'  GROUP by t2.cat_id";
								$query1 = mysqli_query($conn,$sql1); 
								while($gen1 = mysqli_fetch_array($query1)) {?>
						<option value="<?php echo $gen1["cat_id"]?>"><?php echo $gen1["cat_name"]?></option>
							<?php } ?>
						</select>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
						<label >ชื่อทรัพย์สิน*</label>
						<select name="pro_id" id="pro_id" class="form-control" required>
						<option value="">-ชื่อทรัพย์สิน-</option>
						</select>
			</div>
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
						<?php
						$list_datebo = date("Y-m-d");
						?>
						<label >วันที่ยืม *</label>
						<input class="form-control" type="Date" id="datebo" name="datebo" max="<?php echo date('Y-m-d');?>" required>
			</div>
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
						<label >วันที่คืน *</label>
						<input class="form-control" type="Date" id="datere" name="datere" min="<?php echo date('Y-m-d');?>" required>
			</div>
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
						<label >หมายเหตุ </label>
						<input class="form-control" type="Text" id="note" name="note">
			</div>
		</div>
			<div class="submit-button text-center">
					<button name="btnSubmit" type="submit" value="Submit"  class="btn btn-success "><i class="fa fa-save"></i> </button>
                    <button type="reset" name="btn_reset" id="btn_reset" class="btn btn-danger "><i class="fa fa-trash"></i> </button>
			</div>

		</form>	
	</div>
	<!-- จบหมวดหมู่ -->
	
	<br>
	<!-- Start Footer -->
	<footer class="">
		<div class="copyright">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<p class="company-name">  &copy; 2021 ID Drives. Co.,Ltd
					
					</div>
				</div>
			</div>
		</div>
		
	</footer>
	<!-- End Footer -->
	<script>
		function getProperty() {
			$("#pro_id").html(`
				<option value="">-ชื่อทรัพย์สิน-</option>
				`);
			let cat_id = $("#cat_id").val();
			let br_id = $("#br_id").val();
			let dep_id = $("#dep_id").val();


    $.ajax({
        type: "POST",
        url: "./php/getproperty_by_cat_json.php",
        data: JSON.stringify({
                "cat_id": cat_id,
                "br_id":br_id,
                "dep_id":dep_id,
            }),
        success: function(result) {
			console.log(result);
			if(result['datas'] !== null){
				for(let i=0;i<result['datas'].length;i++){
					$("#pro_id").append(`
				<option value="${result['datas'][i]['pro_id']}">${result['datas'][i]['pro_name']}</option>
				`);
				}

			}else{
				alert("ไม่พบข้อมูลทัพย์สิน")
			}
        }
    });
};
	</script>
	<!--  <a href="#" id="back-to-top" title="Back to top" style="display: none;"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></a>   -->

	<!-- ALL JS FILES -->
	<!-- <script src="js/jquery-3.2.1.min.js"></script> -->
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
	<script src="js/jquery.superslides.min.js"></script>
	<script src="js/images-loded.min.js"></script>
	<script src="js/isotope.min.js"></script>
	<script src="js/baguetteBox.min.js"></script>
	<script src="js/form-validator.min.js"></script>
    <script src="js/contact-form-script.js"></script>
    <!-- <script src="js/custom.js"></script> -->
</body>
</html>

