<?php
    session_start();
	if (!$_SESSION['ad_user']){
		header("Location: /iddrivesgit/index.php");
	}
	require('config/conn.php');
$generateFilename = "ทรัพย์สิน" . date("Ymd") . "-" . random_int(1, 100);

header("Content-Type: application/vnd.ms-excel"); // ประเภทของไฟล์
header("Content-Disposition: attachment; filename=".$generateFilename.".xls "); //กำหนดชื่อไฟล์
header("Content-Type: application/force-download"); // กำหนดให้ถ้าเปิดหน้านี้ให้ดาวน์โหลดไฟล์
header("Content-Type: application/octet-stream"); 
header("Content-Type: application/download"); // กำหนดให้ถ้าเปิดหน้านี้ให้ดาวน์โหลดไฟล์
header("Content-Transfer-Encoding: binary"); 
header("Content-Length: ".@filesize($generateFilename));   
@readfile($filename); 

$ad_userName = $_SESSION['ad_user'];
$sqlUser = "SELECT admin.ad_name,department.dep_name,department.dep_id,branch.br_id,branch.br_name FROM admin INNER JOIN department INNER JOIN branch ON admin.dep_id = department.dep_id and admin.br_id=branch.br_id WHERE ad_user='$ad_userName' ";
$resultsqlUser = mysqli_query($conn, $sqlUser);
$num_rows = mysqli_num_rows($resultsqlUser);
$rowsqlUser =  mysqli_fetch_array($resultsqlUser);
$ad_name = $rowsqlUser["ad_name"];
$dep_name = $rowsqlUser["dep_name"];
$dep_id = $rowsqlUser["dep_id"];
$br_id = $rowsqlUser["br_id"];
$br_name = $rowsqlUser["br_name"];

$SQLProperty = "SELECT * FROM property 
LEFT JOIN category ON category.cat_id = property.cat_id  
LEFT JOIN department ON department.dep_id = property.dep_id
WHERE property.dep_id='$dep_id'and pro_status NOT IN ('1')";
$rows=mysqli_query($conn,$SQLProperty);

?>
<html xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
	<table x:str class="table table-striped table-bordered" border="1" >
			<thead>
				<tr class="text-center">
					<th>รหัสทรัพย์สิน</th>
					<th>รายการ</th>
					<th>ราคา(บาท)</th>
					<th>วันที่ได้มา</th>
					<th>ประเภท</th>
					<th>สาขา</th>
					<th>แผนก</th>
					<th>สถานะ</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($rows as $row) { 
					
					?>
				<tr>
					<td style="width:5%" class="text-center"><?php echo $row['pro_id']; ?></td>
					<td style="width:15%" class="text-center"><?php echo $row['pro_name']; ?> <?php echo $row['pro_detail']; ?> <?php echo $row['pro_color']; ?></td>
					<td style="width:15%" class="text-center"><?php echo number_format( $row['pro_price'],2); ?></td>
					<td style="width:15%" class="text-center"><?php echo $row['pro_date']; ?></td>
					<td style="width:10%" class="text-center"><?php echo $row['cat_name']; ?></td>
					<td style="width:10%" class="text-center"><?php echo $br_name; ?></td>
					<td style="width:8%" class="text-center"><?php echo $row['dep_name']; ?></td>
					<td style="width:8%" class="text-center">
					<?php
									if ($row["pro_status"] == "0") {
										$level = "ใช้งานได้";
										echo "<font color=\"green\">$level</font>"; 
									} else if ($row["pro_status"] == "1") {
										$level = "ชำรุด";
										echo "<font color=\"red\">$level</font>"; 
									} else if ($row["pro_status"] == "2"){
										$level = "กำลังถูกยืม";
										echo "<font color=\"red\">$level</font>"; 
									}else{
										$level = "รอการอนุมัติ";
										echo "<font color=\"Orange\">$level</font>"; 
									}
									?>
					</td>
				</tr>
			<?php }?>
			</tbody>
			
		</table>
</body>
</html>
<?php

// echo '<script> location.close(); </script>'
?>