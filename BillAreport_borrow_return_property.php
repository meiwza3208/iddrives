<?php
 session_start();
 if (!$_SESSION['ad_user']){
	 header("Location: /iddrivesgit/index.php");
 }
require('config/conn.php');

@$daystart=$_GET['daystart'];
@$dayend= $_GET['dayend'];
@$bo_status= $_GET['bo_status'];
@$dep_id= $_GET['dep_id'];
?>

<?php

$ad_userName = $_SESSION['ad_user'];
$sqlUser = "SELECT admin.ad_name,department.dep_name,department.dep_id,branch.br_id,branch.br_name FROM admin INNER JOIN department INNER JOIN branch ON admin.dep_id = department.dep_id and admin.br_id=branch.br_id WHERE ad_user='$ad_userName' ";
$resultsqlUser = mysqli_query($conn, $sqlUser);
$num_rows = mysqli_num_rows($resultsqlUser);
$rowsqlUser =  mysqli_fetch_array($resultsqlUser);
$ad_name = $rowsqlUser["ad_name"];
$dep_name = $rowsqlUser["dep_name"];
$dep_id = $rowsqlUser["dep_id"];
$br_id = $rowsqlUser["br_id"];
$br_name = $rowsqlUser["br_name"];

?>
<?php
// Require composer autoload
require_once __DIR__ . '/vendor/autoload.php';

$defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
$fontData = $defaultFontConfig['fontdata'];
$mpdf = new \Mpdf\Mpdf(['tempDir' => __DIR__ . '/tmp',
    'fontdata' => $fontData + [
            'sarabun' => [
                'R' => 'THSarabunNew.ttf',
                'I' => 'THSarabunNewItalic.ttf',
                'B' =>  'THSarabunNewBold.ttf',
                'BI' => "THSarabunNewBoldItalic.ttf",
            ]
        ],
]);

ob_start(); // Start get HTML code
?>



<!DOCTYPE html>
<html>
<head>
<title>PDF</title>
<link href="https://fonts.googleapis.com/css?family=Sarabun&display=swap" rel="stylesheet">
<style>
body {
    font-family: sarabun;
}
table {
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: center;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>

<style type="text/css" media="screen">
#paper
{
	background: #FFF;
	border: 1px solid #666;
	margin: 20px auto;
	width: 21cm;
	min-height: 25cm;
	padding: 50px;
	position: relative;
	
	/* CSS3 */
	
	box-shadow: 0px 0px 5px #000;
	-moz-box-shadow: 0px 0px 5px #000;
	-webkit-box-shadow: 0px 0px 5px #000;
}
</style>
<style type="text/css" >

#paper textarea
{
	margin-bottom:25px;
	width: 50%;
}

#paper table, #paper th, #paper td { border: 10; }

#paper table.border, #paper table.border th, #paper table.border td { border: 1px solid #666; }

#paper th
{
	background: none;
	color: #000
}

#paper hr { border-style: solid; }

#signature
{
	bottom: 181px;
	margin: 50px;
	padding: 50px;
	position: absolute;
	right: 3px;
	text-align: center;
}
</style>
<div style="text-align: center; ">
    <a href="fmAreport_borrow_return_property.php"><button>กลับหน้ารายงาน</button></a>
    <a href="MyPDF.pdf"><button>พิมพ์ใบรายงาน</button></a>
<div>
</head>
<body>
    <script>
            function searchdata(){
            document.forms["frmSearch"].action="fmAreport_borrow_return_property_search.php";
            }
    </script>
<div id="paper">
  <div align="right">
    <?php
      $date = date("Y-m-d");
    ?>
    รายงาน ณ วันที่ : <?php echo $date ?> 
      <?php
        date_default_timezone_set("Asia/Bangkok");
        echo date ('H:i:s');
          ?>
	</div>
    <table  width="100%">
        <tr>
            <td><img src="images/Logo.png" width="100" height="100"></a></td>
                
            <td><h2>บริษัท ไอดีไดร์ฟ จำกัด </h2>
                    <h3>200/222 ถนนชัยพฤกษ์ ตำบลในเมือง <br>
                    อำเภอเมือง จังหวัดขอนแก่น 40000<br>
                    โทร.043 228 899</h3>
            </td>
               
            <td width="30%">
            </td>    
        </tr>
    </table>
	<table  width="100%">
        <tr>
            <td width="30%" ></a></td>
                
            <td width="30%">
            <h2 align="center" style="padding-top:-20px;"><b>รายงานการยืม-คืนทรัพย์สิน

            <?php 
                if($bo_status=="2"){?>
                <span >(รายการทั้งหมด)</span>
                <?php  }?>
                <?php  if($bo_status=="0"){?>
                <span >(เฉพาะรายการถูกคืนแล้ว)</span>
                <?php  }?>
                <?php  if($bo_status=="1"){?>
                <span >(เฉพาะรายการกำลังถูกยืม)</span>
            <?php  }?>

            </td>
               
            <td width="30%">
            </td>    
        </tr>
  </table>
  <form name="frmSearch" method="GET">    
          <?php if($daystart !="" && $dayend !=""){
          ?>
            <b>ระหว่างวันที่ : <?php echo $daystart;?> ถึง <?php echo $dayend;?></b>
          <?php }else{ ?>
          
            <?php } ?>
  </form>
	<?php
         if($bo_status=="2"){
			$bo_status = "";
         }

         if($daystart !="" && $dayend !=""){
			$sql="SELECT DISTINCT borrow_property.datebo FROM borrow_property 
			WHERE borrow_property.bo_status LIKE '%$bo_status%'
			AND datebo BETWEEN '$daystart'  AND '$dayend'";
			
		   }else{
			$sql="SELECT DISTINCT borrow_property.datebo FROM borrow_property 
			WHERE borrow_property.bo_status LIKE '%$bo_status%'
			ORDER BY borrow_property.datebo ASC";
		   }
         
		 @$result = mysqli_query(@$conn,$sql);
          if(@$result->num_rows>0){
          ///ตัวแปรเอาไว้คำนวณ
          //$totalnum = 0;
          //$totalprice = 0;
         // $totallist=0;
         $no0 = 0; //ยังไม่ติดตั้ง
         $no1 = 0; //ติดตั้งแล้ว
         $totalpricelist = 0;
         $totallist = 0;
          ///
        while($data = mysqli_fetch_assoc($result)){ 
            $datebo = $data['datebo'];
        ?>
        <b>วันที่ : <?php echo $data['datebo'];?></b>
        
        <table class="table table-striped">
              <thead>
              <tr align="center" >
                  <th>ลำดับ</th>
                  <th>ชื่อ-สกุลผู้ยืม</th>
                  <th>รายการ (ชื่อ รายละเอียด สี)</th>
                  <th>สาขา</th>
					        <th>แผนก</th>
                  <th>วันที่ยืม</th>
                  <th>วันที่คืน</th>
            
                <?php
                if($bo_status=="0" AND $bo_status=="1" AND $bo_status=="2" ){
                ?>
                <?php
                }
                ?>
                <?php
                if($bo_status==""){
                ?>
                <th>สถานะ</th>
                <?php
                }
                ?>
              </tr>
            </thead>

            <?php
            $no = 1;
            $sql1="SELECT branch.br_name,department.dep_name,property.pro_name,property.pro_detail,
			property.pro_color,borrow_property.bo_name,borrow_property.datebo,borrow_property.bo_status,
			borrow_property.bo_id,
			return_property.re_date FROM branch INNER JOIN department ON department.br_id=branch.br_id 
			INNER JOIN property ON property.dep_id=department.dep_id INNER JOIN borrow_property 
			ON borrow_property.pro_id=property.pro_id LEFT JOIN return_property ON 
			borrow_property.bo_id=return_property.bo_id 
			WHERE borrow_property.datebo= '$datebo' 
            AND borrow_property.bo_status LIKE '%$bo_status%'
            ORDER BY borrow_property.bo_id";
            @$result1 = mysqli_query(@$conn,$sql1);
            if(@$result1->num_rows>0){
               $totalnum = 0;
               $totalprice = 0;
           	while($data1 = mysqli_fetch_assoc($result1)){ 
          ?>
          <tr align="center" >
              <td style="width:10%"><?php echo $no; ?></td>
			  <td style="width:10%" class="text-center"><?php echo $data1['bo_name']; ?></td>
			  <td style="width:20%"><?php echo $data1['pro_name']; ?> <?php echo $data1['pro_detail']; ?> <?php echo $data1['pro_color']; ?></td>
        <td style="width:20%" class="text-center"><?php echo $data1['br_name']; ?></td>
				<td style="width:10%" class="text-center"><?php echo $data1['dep_name']; ?></td>
			  <td style="width:10%" class="text-center"><?php echo $data1['datebo']; ?></td>
			  <td style="width:10%" class="text-center">
								<?php
									if($data1["re_date"]== null){
									$data="กำลังถูกยืม";
									echo "<font color=\"red\">$data</font>";
								 }else{
									$data = $data1['re_date'];
									echo "$data";
								 }
								?>
						
							</td>

             <?php if($bo_status==""){ ?>
             <td style="width:10%" class="text-center"><?php 
              if($data1['bo_status']=="0"){
                $no0 = $no0+1;
                echo "<font color=\"green\">ถูกคืนแล้ว</font>";
              } 
              if($data1['bo_status']=="1"){
                $no1 = $no1+1;
               echo "<font color=\"red\">กำลังถูกยืม</font>";
              } 
            
              ?></td>
              <?php   }   ?>
          </tr>

          <?php 
			$no ++; }}
			@$totalnum = (@$totalnum + $no)-1;
			@$totallist = (@$totallist + $no) -1;
          ?>
        </table> 
<!--  -->

        <div class="row">
			<div class="col-md-8">

			</div>
          <div class="col-md-4" align="right">
          <a>รวม : <?php echo $totalnum; ?> รายการ </a><br>
          
          
          </div>
      </div>
<!--  -->
          <br><br>
        <?php
            }}
        
        ?>
        <div align="right">
			<?php if(@$totallist!=null){ ?>

				<b>รวมทั้งหมด : <?php echo @$totallist; ?>  รายการ  </b><br>
				
			
			<?php }else{
                echo "ไม่พบข้อมูลรายงาน";
            }  ?>
        </div>


        

<!--****************************************************************************************************************-->
    <?php if($bo_status=="0" AND $bo_status=="1" ){ ?>
         
         <?php
          }
          ?>
          <?php
          if($bo_status==""){
          ?>
          <b>ทรัพย์สินที่กำลังถูกยืม :  <?php echo @$no1; ?> รายการ  </b><br>
          <b>ทรัพย์สินที่ถูกคืนแล้ว : <?php echo @$no0; ?> รายการ  </b><br>
          
    <?php } ?>
</div>

</body>
</head>
</html>


<?php
$html = ob_get_contents();
$mpdf->WriteHTML($html);
$mpdf->Output("MyPDF.pdf");
ob_end_flush()
?>


