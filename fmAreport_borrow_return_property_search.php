<?php
    session_start();
	if (!$_SESSION['ad_user']){
		header("Location: /iddrivesgit/index.php");
	}
	require('config/conn.php');
?>

<?php


@$daystart=$_GET['daystart'];
@$dayend= $_GET['dayend'];
@$bo_status= $_GET['bo_status'];
@$dep_id= $_GET['dep_id'];
?>

<!DOCTYPE html>
<html lang="en"><!-- Basic -->
<head>

	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">   
   
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
 
     <!-- Site Metas -->
   <title>ADMIN</title>  
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="images/logo1.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
	
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">    
	<!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css">    
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
		

</head>
<style>
.datadepartment_filter {
	text-align: end !important;
}
#datadepartment_filter {
	text-align: end !important;
}
</style>
<script>
		$(document).ready(function() {
	    	$('#datadepartment').DataTable();
		} );
</script>

<!--start real time-->
<?php

$ad_userName = $_SESSION['ad_user'];
$sqlUser = "SELECT admin.ad_name,department.dep_name,department.dep_id,branch.br_id,branch.br_name FROM admin INNER JOIN department INNER JOIN branch ON admin.dep_id = department.dep_id and admin.br_id=branch.br_id WHERE ad_user='$ad_userName' ";
$resultsqlUser = mysqli_query($conn, $sqlUser);
$num_rows = mysqli_num_rows($resultsqlUser);
$rowsqlUser =  mysqli_fetch_array($resultsqlUser);
$ad_name = $rowsqlUser["ad_name"];
$dep_name = $rowsqlUser["dep_name"];
$dep_id = $rowsqlUser["dep_id"];
$br_id = $rowsqlUser["br_id"];
$br_name = $rowsqlUser["br_name"];



?>
<!--end real time-->

<body>
	<!-- เริ่มดัก ERROR -->
	<div>
		<?php if (isset($_SESSION["process_success"])) : ?>
			<div class="alert alert-success">
				<?php echo $_SESSION["process_success"];
				unset($_SESSION["process_success"]);
				?>

			</div>
		<?php elseif (isset($_SESSION["process_error"])) : ?>
			<div class="alert alert-danger">
				<?php echo $_SESSION["process_error"];
				unset($_SESSION["process_error"]);
				?>
			</div>
		<?php endif ?>
	</div>
	<!-- จบดัก ERROR -->
	<!-- Start header -->
	<header class="top-navbar">
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<div class="container">
				<a class="navbar-brand">
					<a href="fmadmin.php" ><img src="images/Logo.png"class="rounded-circle" alt="Cinque Terre" width="100"/>
				</a>
			
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbars-rs-food" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
				  <span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbars-rs-food">
				<ul class="navbar-nav ml-auto">
						<li class="nav-item active"><a class="nav-link" href="fmadmin.php">หน้าหลัก</a></li>
						<li class="nav-item "><a class="nav-link" href="fmbranch.php">ข้อมูลสาขา</a></li>						
						<li class="nav-item "><a class="nav-link" href="fmdepartment.php">ข้อมูลแผนก</a></li>
						<li class="nav-item "><a class="nav-link" href="fmuser.php">ข้อมูลผู้ใช้ระบบ</a></li>
						<li class="nav-item "><a class="nav-link" href="fmcategory.php">ข้อมูลประเภท</a></li>
						<li class="nav-item dropdown ">
						<a class="nav-link dropdown-toggle" href="#" id="dropdown-a" data-toggle="dropdown">ข้อมูลทรัพย์สิน</a>
							<div class="dropdown-menu" aria-labelledby="dropdown-a">
							<a class="dropdown-item " href="fmproperty.php">ทรัพย์สิน</a>
								<a class="dropdown-item " href="fmpropertyAdd.php">ยืนยันการเพิ่มทรัพย์สิน</a>
							</div>
						</li>
						<a class="nav-link">|สวัสดี:<?php echo $ad_name;?></a><a class="nav-link" href="php\logout.php"><img src="images/iconlogout.png" width="30"><br></a>
					</ul>
				</div>
			</div>
		</nav>
	</header>
	<!-- End header -->

	<!-- Start All Pages -->
	<div class="all-page-title1 page-breadcrumb1">
		<div class="container text-center">
			<div class="row">
				<div class="col-lg-12">
				</div>
			</div>
		</div>
	</div>
	<!-- End All Pages -->

    <!-- เริ่มหมวดหมู่ -->
	
    <div class="container">	
    <h2 style="font-size:30px" align="center"></img> รายงานการยืมคืนทรัพย์สิน
		<?php 
			if($bo_status=="2"){?>
			<span >(รายการทั้งหมด)</span>
			<?php  }?>
			<?php  if($bo_status=="0"){?>
			<span >(เฉพาะรายการถูกคืนแล้ว)</span>
			<?php  }?>
			<?php  if($bo_status=="1"){?>
			<span >(เฉพาะรายการกำลังถูกยืม)</span>
		<?php  }?>
	</h2>  

	<script>
            function searchdata(){
            document.forms["frmSearch"].action="fmAreport_borrow_return_property_search.php";
            }
    </script>

		<form name="frmSearch" method="GET">

			<div class="row"> 
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
				<?php if($daystart !="" && $dayend !=""){?>
					<label>ระหว่างวันที่ยืม</label>
					<input type="date"  name="daystart" id="daystart" class="form-control" value ="<?php echo $daystart;?>" max="<?php echo date('Y-m-d');?>">ถึง วันที่ยืม<input type="date" class="form-control" name="dayend" id="dayend" value ="<?php echo $dayend;?>"max="<?php echo date('Y-m-d');?>">
				<?php }else{ ?>
					<label>ถึง  ระหว่างวันที่ยืม</label>
					<input type="date" name="daystart" id="daystart" class="form-control " max="<?php echo date('Y-m-d');?>"required><input type="date"  name="dayend" id="dayend" class="form-control" max="<?php echo date('Y-m-d');?>"required>
				<?php } ?>

			    </div>

            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
					<label>สถานะ</label>
					<select name="bo_status" id="bo_status"class="form-control">
						<?php  if($bo_status=="0"){?>
							<option value="0"selected>ถูกคืนแล้ว</option>
							<option value="1">กำลังถูกยืม</option>
							<option value="2">ทั้งหมด</option>
						<?php  }?>
						<?php  if($bo_status=="1"){?>
							<option value="0">ถูกคืนแล้ว</option>
							<option value="1"selected>กำลังถูกยืม</option>
							<option value="2">ทั้งหมด</option>
						<?php  }?>
						<?php  if($bo_status=="2"){?>
							<option value="0">ถูกคืนแล้ว</option>
							<option value="1">กำลังถูกยืม</option>
							<option value="2" selected>ทั้งหมด</option>
						<?php  }?>
	
					</select>
                </div>
			</div>
      
            <div class="text-center">
	            <br><button class="btn btn-success"type="submit" value="Search" onclick="searchdata()">ค้นหา</button></a> 
		    </div>
			<div class="text-right">
			<p
									daystart="<?php echo $daystart; ?>";
                                    dayend="<?php echo $dayend; ?>";
                                    bo_status="<?php echo $bo_status; ?>";
                                    onclick="printreceives(this)"style="cursor:pointer;"
                                    target="_blank"><img src="images/print.png" wigth="100px" height="100px"></p>	
			</div>
		</form>
		
		<script>
   function printreceives(obj){
          var daystart = obj.getAttribute("daystart");
          var dayend = obj.getAttribute("dayend");
          var bo_status = obj.getAttribute("bo_status");
          
         // console.log(dayin);
		 window.location.href="BillAreport_borrow_return_property.php?daystart="+daystart+"&dayend="+dayend+"&bo_status="+bo_status;
  
	}
		</script>	
	
        <?php
         if($bo_status=="2"){
			$bo_status = "";
         }

         if($daystart !="" && $dayend !=""){
			$sql="SELECT DISTINCT borrow_property.datebo FROM borrow_property 
			WHERE borrow_property.bo_status LIKE '%$bo_status%'
			AND datebo BETWEEN '$daystart'  AND '$dayend'";
			
		   }else{
			$sql="SELECT DISTINCT borrow_property.datebo FROM borrow_property 
			WHERE borrow_property.bo_status LIKE '%$bo_status%'
			ORDER BY borrow_property.datebo ASC";
		   }
		 
         
		 @$result = mysqli_query(@$conn,$sql);
          if(@$result->num_rows>0){
          ///ตัวแปรเอาไว้คำนวณ
          //$totalnum = 0;
          //$totalprice = 0;
         // $totallist=0;
         $no0 = 0; //ยังไม่ติดตั้ง
         $no1 = 0; //ติดตั้งแล้ว
         $totalpricelist = 0;
         $totallist = 0;
          ///
        while($data = mysqli_fetch_assoc($result)){ 
          $datebo = $data['datebo'];
        ?>

        <b>วันที่ : <?php echo $data['datebo'];?></b>
        <table class="table table-striped">
			<thead>
				<tr align="center" >
					<th>ลำดับ</th>
					<th>ชื่อ-สกุลผู้ยืม</th>
					<th>รายการ (ชื่อ รายละเอียด สี)</th>
					<th>สาขา</th>
					<th>แผนก</th>
					<th>วันที่ยืม</th>
					<th>วันที่คืน</th>
			
					<?php
					if($bo_status=="0" AND $bo_status=="1" AND $bo_status=="2" ){
					?>
					<?php
					}
					?>
					<?php
					if($bo_status==""){
					?>
					<th>สถานะ</th>
					<?php
					}
					?>
				</tr>
			</thead>

          <?php
            $no = 1;
            $sql1="SELECT branch.br_name,department.dep_name,property.pro_name,property.pro_detail,
			property.pro_color,borrow_property.bo_name,borrow_property.datebo,borrow_property.bo_status,
			borrow_property.bo_id,
			return_property.re_date FROM branch INNER JOIN department ON department.br_id=branch.br_id 
			INNER JOIN property ON property.dep_id=department.dep_id INNER JOIN borrow_property 
			ON borrow_property.pro_id=property.pro_id LEFT JOIN return_property ON 
			borrow_property.bo_id=return_property.bo_id 
			WHERE borrow_property.datebo= '$datebo' 
            AND borrow_property.bo_status LIKE '%$bo_status%'
            ORDER BY borrow_property.bo_id";
            @$result1 = mysqli_query(@$conn,$sql1);
            if(@$result1->num_rows>0){
               $totalnum = 0;
               $totalprice = 0;
           	while($data1 = mysqli_fetch_assoc($result1)){ 
          ?>
          <tr align="center" >
              <td style="width:10%"><?php echo $no; ?></td>
			  <td style="width:10%" class="text-center"><?php echo $data1['bo_name']; ?></td>
			  <td style="width:20%"><?php echo $data1['pro_name']; ?> <?php echo $data1['pro_detail']; ?> <?php echo $data1['pro_color']; ?></td>
			  <td style="width:20%" class="text-center"><?php echo $data1['br_name']; ?></td>
			 <td style="width:10%" class="text-center"><?php echo $data1['dep_name']; ?></td>
			  <td style="width:10%" class="text-center"><?php echo $data1['datebo']; ?></td>
			  <td style="width:10%" class="text-center">
								<?php
									if($data1["re_date"]== null){
									$data="กำลังถูกยืม";
									echo "<font color=\"red\">$data</font>";
								 }else{
									$data = $data1['re_date'];
									echo "$data";
								 }
								?>
						
							</td>


             <?php if($bo_status==""){ ?>
             <td style="width:10%" class="text-center"><?php 
              if($data1['bo_status']=="0"){
                $no0 = $no0+1;
                echo "<font color=\"green\">ถูกคืนแล้ว</font>";
              } 
              if($data1['bo_status']=="1"){
                $no1 = $no1+1;
               echo "<font color=\"red\">กำลังถูกยืม</font>";
              } 
            
              ?></td>
              <?php   }   ?>
          </tr>

          <?php 
			$no ++; }}
			@$totalnum = (@$totalnum + $no)-1;
			@$totallist = (@$totallist + $no) -1;
          ?>
        </table> 
<!--  -->

        <div class="row">
			<div class="col-md-8">

			</div>
			<div class="col-md-4" align="right">
			<a>รวม : <?php echo $totalnum; ?> รายการ </a><br>
			
			
			</div>
        </div>
<!--  -->
          <br><br>
        <?php
            }}
        
        ?>
        <div align="right">
			<?php if(@$totallist!=null){ ?>

				<b>รวมทั้งหมด : <?php echo @$totallist; ?>  รายการ  </b><br>
				
			
			<?php }else{
                echo "ไม่พบข้อมูลรายงาน";
            }  ?>
        </div>


        

<!--****************************************************************************************************************-->

		<?php if($bo_status=="0" AND $bo_status=="1" ){ ?>
			<?php
			}
			?>
			<?php
			if($bo_status==""){
			?>
			<b>ทรัพย์สินที่กำลังถูกยืม :  <?php echo @$no1; ?> รายการ  </b><br>
			<b>ทรัพย์สินที่ถูกคืนแล้ว : <?php echo @$no0; ?> รายการ  </b><br>
			
		<?php } ?>
				
</div><br>		

	<!-- จบหมวดหมู่ -->
	
	<br>
	<!-- Start Footer -->
	<footer class="">
		<div class="copyright">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<p class="company-name">  &copy; 2021 ID Drives. Co.,Ltd
					
					</div>
				</div>
			</div>
		</div>
		
	</footer>
	<!-- End Footer -->
	
	<!--  <a href="#" id="back-to-top" title="Back to top" style="display: none;"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></a>   -->

	<!-- ALL JS FILES -->
	<!-- <script src="js/jquery-3.2.1.min.js"></script> -->
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
	<script src="js/jquery.superslides.min.js"></script>
	<script src="js/images-loded.min.js"></script>
	<script src="js/isotope.min.js"></script>
	<script src="js/baguetteBox.min.js"></script>
	<script src="js/form-validator.min.js"></script>
    <script src="js/contact-form-script.js"></script>
    <!-- <script src="js/custom.js"></script> -->
</body>
</html>

