<?php
    session_start();
	if (!$_SESSION['ad_user']){
		header("Location: /iddrivesgit/index.php");
	}
	require('config/conn.php');
?>

<?php

if (isset($_REQUEST["id"])) {
    $id = $_REQUEST["id"];
    $sql = "SELECT * FROM property WHERE pro_id = '$id'";
    $result = mysqli_query($conn, $sql);
    if (mysqli_num_rows($result) == 1) {
        $data = mysqli_fetch_assoc($result);
    } else {
        header("location:../fmcategory.php");
    }
}
?>

<!DOCTYPE html>
<html lang="en"><!-- Basic -->
<head>

	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">   
   
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
 
     <!-- Site Metas -->
    <title>STAFF</title>  
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="images/logo1.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
	
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">    
	<!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css">    
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
		

</head>
<style>
.datadepartment_filter {
	text-align: end !important;
}
#datadepartment_filter {
	text-align: end !important;
}
</style>
<script>
		$(document).ready(function() {
	    	$('#datadepartment').DataTable();
		} );
</script>

<!--start real time-->
<?php

$ad_userName = $_SESSION['ad_user'];
$sqlUser = "SELECT admin.ad_name,department.dep_name,department.dep_id,branch.br_id,branch.br_name FROM admin INNER JOIN department INNER JOIN branch ON admin.dep_id = department.dep_id and admin.br_id=branch.br_id WHERE ad_user='$ad_userName' ";
$resultsqlUser = mysqli_query($conn, $sqlUser);
$num_rows = mysqli_num_rows($resultsqlUser);
$rowsqlUser =  mysqli_fetch_array($resultsqlUser);
$ad_name = $rowsqlUser["ad_name"];
$dep_name = $rowsqlUser["dep_name"];
$dep_id = $rowsqlUser["dep_id"];
$br_id = $rowsqlUser["br_id"];
$br_name = $rowsqlUser["br_name"];

?>
<!--end real time-->

<body>
	<!-- เริ่มดัก ERROR -->
	<div>
		<?php if (isset($_SESSION["process_success"])) : ?>
			<div class="alert alert-success">
				<?php echo $_SESSION["process_success"];
				unset($_SESSION["process_success"]);
				?>

			</div>
		<?php elseif (isset($_SESSION["process_error"])) : ?>
			<div class="alert alert-danger">
				<?php echo $_SESSION["process_error"];
				unset($_SESSION["process_error"]);
				?>
			</div>
		<?php endif ?>
	</div>
	<!-- จบดัก ERROR -->
	<!-- Start header -->
	<header class="top-navbar">
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<div class="container">
				<a class="navbar-brand">
					<a href="staff.php" ><img src="images/Logo.png"class="rounded-circle" alt="Cinque Terre" width="100"/>
				</a>
			
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbars-rs-food" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
				  <span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbars-rs-food">
					<ul class="navbar-nav ml-auto">
						<li class="nav-item "><a class="nav-link" href="staff.php">หน้าหลัก</a></li>
						<li class="nav-item active"><a class="nav-link" href="Sproperty.php">ข้อมูลทรัพย์สิน</a></li>
						<li class="nav-item "><a class="nav-link" href="Sborrow.php">ยืมทรัพย์สิน</a></li>
						<li class="nav-item "><a class="nav-link" href="Sreturn.php">คืนทรัพย์สิน</a></li>
						<a class="nav-link">พนักงาน: คุณ<?php echo $ad_name;?></a><a class="nav-link" href="php\logout.php"><img src="images/iconlogout.png" width="30"><br></a>
					</ul>
				</div>
			</div>
		</nav>
	</header>
	<!-- End header -->

	<!-- Start All Pages -->
	<div class="all-page-title1 page-breadcrumb1">
		<div class="container text-center">
			<div class="row">
				<div class="col-lg-12">
				</div>
			</div>
		</div>
	</div>
	<!-- End All Pages -->

    <!-- เริ่มหมวดหมู่ -->
	<h1 align="center">แก้ไขข้อมูลทรัพย์สิน</h1>
	<h3 align="center"><font color="red">หมายเหตุ *หากข้อมูลที่เพิ่มผิดพลาดให้ติดต่อที่สำนักงานใหญ่ </font></h3>
	<div class="container">
	
		<form action="php/Supdateproperty.php" method="post" enctype="multipart/form-data">		
				<div>
					<?php if(isset($_SESSION["St_User_error"])) : ?>
						<label style="color: red;">
							<?php echo $_SESSION["St_User_error"];
								unset($_SESSION["St_User_error"]);
							?>
					<?php endif ?>
				</div>

            <div class="row">
				
			<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
					<label >สาขา *</label>
					<input class="form-control" type="Text" id="br_name" name="br_name" value = "<?php echo $rowsqlUser['br_name'];?>"disabled>
					<input class="form-control" type="hidden" id="br_id" name="br_id" value = "<?php echo $rowsqlUser['br_id'];?>">
					<div class="help-block with-errors"></div>
				</div> 
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
					<label>แผนก *</label>
					<input class="form-control" type="Text" id="dep_name" name="dep_name" value = "<?php echo $rowsqlUser['dep_name'];?>"disabled>
					<input class="form-control" type="hidden" id="dep_id" name="dep_id" value = "<?php echo $rowsqlUser['dep_id'];?>">
				</div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
				<input class="form-control" type="hidden" id="cat_id" name="cat_id" value="<?php echo $data["cat_id"]?>"">
					<label >ประเภท *</label>
					<select name="cat_id" class="form-control"disabled>
					<option value="">-ประเภท-</option>
						<?php 
							$sql1 = "SELECT * FROM category";
							$query1 = mysqli_query($conn,$sql1); 
							while($gen1 = mysqli_fetch_array($query1)) {?>
						<?php 
							if($gen1["cat_id"] == $data['cat_id']){
							?>
                        <option value="<?php echo $gen1["cat_id"]?>" selected><?php echo $gen1["cat_name"]?></option>
							<?php 
							}else{
							?>
                        
							<?php 
							}
							?>

						<?php } ?>
				</select>
				</div>
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
					<label >รหัสทรัพย์สิน *</label>
					<input class="form-control" type="Text" id="pro_id" name="pro_id" value="<?php echo $data['pro_id'];?>"disabled>
					<input class="form-control" type="hidden" id="pro_id" name="pro_id" value="<?php echo $data['pro_id'];?>"">
				</div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
					<label >ชื่อทรัพย์สิน *</label>
					<input class="form-control" type="Text" id="pro_name" name="pro_name" value="<?php echo $data['pro_name'];?>"disabled>
					<input class="form-control" type="hidden" id="pro_name" name="pro_name" value="<?php echo $data['pro_name'];?>">
					
				</div>
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
					<label >รายละเอียด(รุ่น/ยี่ห้อ) *</label>
					<input class="form-control" type="Text" id="pro_detail" name="pro_detail" value="<?php echo $data['pro_detail'];?>"disabled>
					<input class="form-control" type="hidden" id="pro_detail" name="pro_detail" value="<?php echo $data['pro_detail'];?>">
					
				</div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
					<label >สี *</label>
					<input class="form-control" type="Text" id="pro_color" name="pro_color" value="<?php echo $data['pro_color'];?>" disabled>
					<input class="form-control" type="hidden" id="pro_color" name="pro_color" value="<?php echo $data['pro_color'];?>">
					
				</div>
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
					<label >ว/ด/ป ที่ได้มา *</label>
					<input class="form-control" type="Text" id="pro_date" name="pro_date" value="<?php echo $data['pro_date'];?>" disabled>
					<input class="form-control" type="hidden" id="pro_date" name="pro_date" value="<?php echo $data['pro_date'];?>">
					
				</div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
					<label >มูลค่า(บาท) *</label>
					<input class="form-control" type="Text" id="pro_price" name="pro_price" value="<?php echo $data['pro_price'];?>" disabled>
					<input class="form-control" type="hidden" id="pro_price" name="pro_price" value="<?php echo $data['pro_price'];?>">
					
				</div>
                <br>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                <label >รูปภาพปัจจุบัน</label>
                <img src="./php/upload/<?php echo $data['pro_pic']; ?>" width="50%"class="form-control"disabled>
                </div>
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
					<label >รูปภาพ</label>
					<input class="form-control" type="file" id="pro_pic" name="pro_pic" >
				</div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12" >
					<label>สภาพทรัพย์สิน *</label><br>
					<input type="hidden"  name="pro_status" value="0" <?php if($data['pro_status']=="0"){?>checked <?php }?> disabled>
         
                    <input type="radio"  name="pro_status" value="1" <?php if($data['pro_status']=="1"){?>checked <?php }?> > 
                    <label>ชำรุด</label>
					<input type="hidden"  name="pro_status" value="2" <?php if($data['pro_status']=="2"){?>checked <?php }?> disabled>
					
              
			    </div>
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-12">
					<label>หมายเหตุ </label>
                    <input class="form-control" type="Text" id="pro_note" name="pro_note" value="<?php echo $data['pro_note'];?>"disabled>
					<input class="form-control" type="hidden" id="pro_note" name="pro_note" value="<?php echo $data['pro_note'];?>">
	
                    </input>
			    </div>
			</div>
								
				<div class="submit-button text-center">
					<button name="btnSubmit" type="submit" value="Submit"  class="btn btn-success "><i class="fa fa-save"></i> </button>
					<button type="reset" name="btn_reset" id="btn_reset"  class="btn btn-danger "><i class="fa fa-trash"></i> </button>
				</div>

				<br>
			</form>
			
	</div>
	<!-- จบหมวดหมู่ -->
	
	<br>
	<!-- Start Footer -->
	<footer class="">
		<div class="copyright">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<p class="company-name">  &copy; 2021 ID Drives. Co.,Ltd
					
					</div>
				</div>
			</div>
		</div>
		
	</footer>
	<!-- End Footer -->
	<script>
		function getDepartment() {
			$("#dep_id").html(`
				<option value="">-แผนก-</option>
				`);
			let br_id = $("#br_id").val();
    $.ajax({
        type: "POST",
        url: "./php/getdepartment_json.php",
        data: JSON.stringify({
                "br_id": br_id,
            }),
        success: function(result) {
			console.log(result);
			if(result['datas'] !== null){
				for(let i=0;i<result['datas'].length;i++){
					$("#dep_id").append(`
				<option value="${result['datas'][i]['dep_id']}">${result['datas'][i]['dep_name']}</option>
				`);
				}
			}else{
				alert("ไม่พบข้อมูลแผนก")
			}
        }
    });
};
	</script>
	<!--  <a href="#" id="back-to-top" title="Back to top" style="display: none;"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></a>   -->

	<!-- ALL JS FILES -->
	<!-- <script src="js/jquery-3.2.1.min.js"></script> -->
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
	<script src="js/jquery.superslides.min.js"></script>
	<script src="js/images-loded.min.js"></script>
	<script src="js/isotope.min.js"></script>
	<script src="js/baguetteBox.min.js"></script>
	<script src="js/form-validator.min.js"></script>
    <script src="js/contact-form-script.js"></script>
    <!-- <script src="js/custom.js"></script> -->
</body>
</html>

