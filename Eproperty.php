<?php
    session_start();
	if (!$_SESSION['ad_user']){
		header("Location: /iddrivesgit/index.php");
	}
	require('config/conn.php');

	$SQLProperty = "SELECT property.pro_date,property.pro_pic,category.cat_name,property.pro_id,property.pro_name,property.pro_detail,property.pro_status,property.pro_color,property.pro_date,property.pro_price,property.pro_note,branch.br_name,department.dep_name FROM property INNER JOIN department INNER JOIN branch INNER JOIN category WHERE property.dep_id=department.dep_id and property.br_id=branch.br_id and property.cat_id=category.cat_id and pro_status NOT IN ('3');";
	$rows=mysqli_query($conn,$SQLProperty);

?>

<!DOCTYPE html>
<html lang="en"><!-- Basic -->
<head>

	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">   
   
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
 
     <!-- Site Metas -->
    <title>ADMIN</title>  
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="images/logo1.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
	
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">    
	<!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css">    
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
		

</head>
<style>
.datadepartment_filter {
	text-align: end !important;
}
#datadepartment_filter {
	text-align: end !important;
}
</style>
<script>
		$(document).ready(function() {
	    	$('#datadepartment').DataTable();
		} );
</script>


<!--start real time-->
<?php

$ad_userName = $_SESSION['ad_user'];
$sqlUser = "SELECT * FROM admin WHERE ad_user='$ad_userName' ";
$resultsqlUser = mysqli_query($conn, $sqlUser);
$num_rows = mysqli_num_rows($resultsqlUser);
$rowsqlUser =  mysqli_fetch_array($resultsqlUser);
$ad_name = $rowsqlUser["ad_name"];

?>
<!--end real time-->

<body>
    <!-- เริ่มดัก ERROR -->
	<div>
		<?php if (isset($_SESSION["process_success"])) : ?>
			<div class="alert alert-success">
				<?php echo $_SESSION["process_success"];
				unset($_SESSION["process_success"]);
				?>

			</div>
		<?php elseif (isset($_SESSION["process_error"])) : ?>
			<div class="alert alert-danger">
				<?php echo $_SESSION["process_error"];
				unset($_SESSION["process_error"]);
				?>
			</div>
		<?php endif ?>
	</div>
	<!-- จบดัก ERROR -->
	<!-- Start header -->
	<header class="top-navbar">
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<div class="container">
				<a class="navbar-brand" href="index.html">
					<a href="executive.php" ><img src="images/Logo.png"class="rounded-circle" alt="Cinque Terre" width="100"/>
				</a>
			
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbars-rs-food" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
				  <span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbars-rs-food">
					<ul class="navbar-nav ml-auto">
					<li class="nav-item  "><a class="nav-link" href="executive.php">หน้าหลัก</a></li>
						<li class="nav-item active "><a class="nav-link" href="Eproperty.php">รายงานทรัพย์สิน</a></li>						
						<li class="nav-item "><a class="nav-link" href="Ereport_borrow_return_property.php">รายงานการยืม-คืนทรัพย์สิน</a></li>
					<a class="nav-link">| ผู้บริหาร : คุณ<?php echo $ad_name;?></a><a class="nav-link" href="php\logout.php"><img src="images/iconlogout.png" width="30"><br></a>
					</ul>
				</div>
			</div>
		</nav>
	</header>
	<!-- End header -->

	<!-- Start All Pages -->
	<div class="all-page-title1 page-breadcrumb1">
		<div class="container text-center">
			<div class="row">
				<div class="col-lg-12">
				</div>
			</div>
		</div>
	</div>
	<!-- End All Pages -->
	
		<!-- เริ่มหมวดหมู่ -->
	<h1 align="center">รายงานข้อมูลทรัพย์สิน</h1>
	<div class="container">
	<div class="text-right mb-2">
		<a class="text-right btn btn-primary text-light" href="fmAexportproperty.php"><img src="./images/excel.png" width="20px"> ดาวน์โหลดไฟล์</a>
		</div>
		<table id="datadepartment" class="table table-striped table-bordered" >
			<thead>
				<tr class="text-center">
					<th>รหัส</th>
					<th>รายการ (ชื่อ รายละเอียด สี)</th>
					<th>ราคา(บาท)</th>
					<th>วันที่ได้มา</th>
					<th>ประเภท</th>
					<th>สาขา</th>
					<th>สถานะ</th>
					<th>รูปภาพ</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($rows as $row) { ?>
				<tr>
				
					<td style="width:4%" class="text-center"><?php echo $row['pro_id']; ?></td>
					<td style="width:18%" class="text-center"><?php echo $row['pro_name']; ?> <?php echo $row['pro_detail']; ?> <?php echo $row['pro_color']; ?></td>
					<td style="width:2%" class="text-center"><?php echo number_format( $row['pro_price'],2); ?></td>
					<td style="width:8%" class="text-center"><?php echo $row['pro_date']; ?></td>
					<td style="width:5%" class="text-center"><?php echo $row['cat_name']; ?></td>
					<td style="width:5%" class="text-center"><?php echo $row['br_name']; ?></td>
					<td style="width:8%" class="text-center">
					<?php
									if ($row["pro_status"] == "0") {
										$level = "ใช้งานได้";
										echo "<font color=\"green\">$level</font>"; 
									} else if ($row["pro_status"] == "1") {
										$level = "ชำรุด";
										echo "<font color=\"red\">$level</font>"; 
									} else if ($row["pro_status"] == "2"){
										$level = "กำลังถูกยืม";
										echo "<font color=\"red\">$level</font>"; 
									}else{
										$level = "รอการอนุมัติ";
										echo "<font color=\"Orange\">$level</font>"; 
									}
					?>
					</td>
					<td style="width:8%" class="text-center">
					<img src="./php/upload/<?php echo $row['pro_pic']; ?>" width="70px" height="70px">
					</td>
				</tr>
			<?php }?>
			</tbody>
			
		</table>
	</div>
	<!-- จบหมวดหมู่ -->

	<!-- Start Footer -->
	<footer class="">
		<div class="copyright">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<p class="company-name">  &copy; 2021 ID Drives. Co.,Ltd
					
					</div>
				</div>
			</div>
		</div>
		
	</footer>
<!-- End Footer -->
	
	<!--  <a href="#" id="back-to-top" title="Back to top" style="display: none;"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></a>   -->

	<!-- ALL JS FILES -->
	<!-- <script src="js/jquery-3.2.1.min.js"></script> -->
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
	<script src="js/jquery.superslides.min.js"></script>
	<script src="js/images-loded.min.js"></script>
	<script src="js/isotope.min.js"></script>
	<script src="js/baguetteBox.min.js"></script>
	<script src="js/form-validator.min.js"></script>
    <script src="js/contact-form-script.js"></script>
    <!-- <script src="js/custom.js"></script> -->
</body>
</html>
