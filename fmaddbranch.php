<?php
    session_start();
	if (!$_SESSION['ad_user']){
		header("Location: /iddrivesgit/index.php");
	}
	require('config/conn.php');

?>

<!DOCTYPE html>
<html lang="en"><!-- Basic -->
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">   
   
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
 
     <!-- Site Metas -->
    <title>ADMIN</title>  
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="images/logo1.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
   
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">    
	<!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css">    
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<!--start real time-->
<?php

$ad_userName = $_SESSION['ad_user'];
$sqlUser = "SELECT * FROM admin WHERE ad_user='$ad_userName' ";
$resultsqlUser = mysqli_query($conn, $sqlUser);
$num_rows = mysqli_num_rows($resultsqlUser);
$rowsqlUser =  mysqli_fetch_array($resultsqlUser);
$ad_name = $rowsqlUser["ad_name"];

?>
<!--end real time-->

<body>
	<!-- เริ่มดัก ERROR -->
	<div>
		<?php if (isset($_SESSION["process_success"])) : ?>
			<div class="alert alert-success">
				<?php echo $_SESSION["process_success"];
				unset($_SESSION["process_success"]);
				?>

			</div>
		<?php elseif (isset($_SESSION["process_error"])) : ?>
			<div class="alert alert-danger">
				<?php echo $_SESSION["process_error"];
				unset($_SESSION["process_error"]);
				?>
			</div>
		<?php endif ?>
	</div>
	<!-- จบดัก ERROR -->
	<!-- Start header -->
	<header class="top-navbar">
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<div class="container">
				<a class="navbar-brand">
					<a href="fmadmin.php" ><img src="images/Logo.png"class="rounded-circle" alt="Cinque Terre" width="100"/>
				</a>
			
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbars-rs-food" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
				  <span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbars-rs-food">
					<ul class="navbar-nav ml-auto">
						<li class="nav-item "><a class="nav-link" href="fmadmin.php">หน้าหลัก</a></li>
						<li class="nav-item active"><a class="nav-link" href="fmbranch.php">ข้อมูลสาขา</a></li>						
						<li class="nav-item "><a class="nav-link" href="fmdepartment.php">ข้อมูลแผนก</a></li>
						<li class="nav-item "><a class="nav-link" href="fmuser.php">ข้อมูลผู้ใช้ระบบ</a></li>
						<li class="nav-item "><a class="nav-link" href="fmcategory.php">ข้อมูลประเภท</a></li>
						<li class="nav-item dropdown ">
						<a class="nav-link dropdown-toggle" href="#" id="dropdown-a" data-toggle="dropdown">ข้อมูลทรัพย์สิน</a>
							<div class="dropdown-menu" aria-labelledby="dropdown-a">
							<a class="dropdown-item " href="fmproperty.php">ทรัพย์สิน</a>
								<a class="dropdown-item " href="fmpropertyAdd.php">ยืนยันการเพิ่มทรัพย์สิน</a>
							</div>
						</li>
						<a class="nav-link">|สวัสดี:<?php echo $ad_name;?></a><a class="nav-link" href="php\logout.php"><img src="images/iconlogout.png" width="30"><br></a>
					</ul>
				</div>
			</div>
		</nav>
	</header>
	<!-- End header -->

	<!-- Start All Pages -->
	<div class="all-page-title1 page-breadcrumb1">
		<div class="container text-center">
			<div class="row">
				<div class="col-lg-12">
				</div>
			</div>
		</div>
	</div>
	<!-- End All Pages -->

	<!-- เริ่มรายการยืมคืน -->
	<h1 align="center">เพิ่มสาขา</h1>
    <br>
    <div class="container">	
	    <form action="php/addbranch.php" method="post">		
		    <div>
                <?php if(isset($_SESSION["St_User_error"])) : ?>
                    <label style="color: red;">
                        <?php echo $_SESSION["St_User_error"];
                            unset($_SESSION["St_User_error"]);
                        ?>
                <?php endif ?>
            </div>
			<div class="row">
				<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
					<label >รหัสสาขา *</label>
					<input class="form-control" type="Text" id="br_id" name="br_id" required>
				</div>
							
				<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
					<label >ชื่อสาขา *</label>
					<input class="form-control" type="Text" id="br_name" name="br_name"required>
				</div>

			</div><br><br><br><br>
            
			
			<div class="submit-button text-center">
				<button type="submit" name="btn_submit" id="btn_submit" value="1" class="btn btn-success "><i class="fa fa-save"></i> </button>
				<button type="reset" name="btn_reset" id="btn_reset" value="1" class="btn btn-danger "><i class="fa fa-trash"></i> </button>
			</div>

			<br>
		</form>
	</div>		
	<!-- จบรายการยืมคืน -->
		
	<br>
	<!-- Start Footer -->
	<footer class="">
		<div class="copyright">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<p class="company-name">  &copy; 2021 ID Drives. Co.,Ltd
					
					</div>
				</div>
			</div>
		</div>
		
	</footer>
	<!-- End Footer -->
	
	<a href="#" id="back-to-top" title="Back to top" style="display: none;"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></a>

	<!-- ALL JS FILES -->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
	<script src="js/jquery.superslides.min.js"></script>
	<script src="js/images-loded.min.js"></script>
	<script src="js/isotope.min.js"></script>
	<script src="js/baguetteBox.min.js"></script>
	<script src="js/form-validator.min.js"></script>
    <script src="js/contact-form-script.js"></script>
    <script src="js/custom.js"></script>
</body>
</html>

